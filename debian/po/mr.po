# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@debian.org>, 2004.
# Priti Patil <prithisd@gmail.com>, 2007.
# Sampada Nakhare, 2007.
# Sandeep Shedmake <sshedmak@redhat.com>, 2009, 2010.
# localuser <sampadanakhare@gmail.com>, 2015.
# Nayan Nakhare <nayannakhare@rediffmail.com>, 2018.
# Prachi Joshi <josprachi@yahoo.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: finish-install@packages.debian.org\n"
"POT-Creation-Date: 2021-12-02 20:02+0000\n"
"PO-Revision-Date: 2023-11-22 04:07+0000\n"
"Last-Translator: omwani <omwani03+gitlab@gmail.com>\n"
"Language-Team: CDAC_DI\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl1:
#: ../finish-install.templates:1001
msgid "Finish the installation"
msgstr "अधिष्ठापना पूर्ण करा"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:2001
msgid "Finishing the installation"
msgstr "अधिष्ठापना पूर्ण करत आहे"

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:3001
msgid "Running ${SCRIPT}..."
msgstr "${SCRIPT} चालवत आहे..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:4001
msgid "Configuring network..."
msgstr "नेटवर्क संरचित केले जातेय..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:5001
msgid "Setting up frame buffer..."
msgstr "फ्रेमबफर निर्धारण होत आहे..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:6001
msgid "Unmounting file systems..."
msgstr "फाइल प्रणाल्या अनारोहित होत आहेत..."

#. Type: text
#. Description
#. :sl1:
#: ../finish-install.templates:7001
msgid "Rebooting into your new system..."
msgstr "आपल्या नवीन प्रणालीत पुनरारंभ करत आहे..."

#. Type: note
#. Description
#. Translators: translate <Continue> identical to the translation of the button!
#. :sl1:
#: ../finish-install.templates:8001
msgid "Installation complete"
msgstr "अधिष्ठापना पूर्ण"

#. Type: note
#. Description
#. Translators: translate <Continue> identical to the translation of the button!
#. :sl1:
#: ../finish-install.templates:8001
msgid ""
"Installation is complete, so it is time to boot into your new system. Make "
"sure to remove the installation media, so that you boot into the new system "
"rather than restarting the installation."
msgstr ""
"अधिष्ठापना पूर्ण झाली आहे. आता आपली नवीन प्रणाली कार्यान्वित करण्याची वेळ आली आहे. "
"अधिष्ठापनेची प्रक्रिया पुन्हा सुरु होऊ नये व आपली नवीन प्रणालीच कार्यान्वित व्हावी म्हणून "
"अधिष्ठापना माध्यम बाहेर काढून घेतल्याची खात्री करा."

#. Type: note
#. Description
#. Translators: translate <Continue> identical to the translation of the button!
#. :sl1:
#: ../finish-install.templates:8001
msgid "Please choose <Continue> to reboot."
msgstr "कृपया रीबूट करण्यासाठी <Continue> निवडा."
